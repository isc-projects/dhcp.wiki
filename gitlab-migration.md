This page lists all necessary steps needed to migrate ISC DHCP to gitlab.

* [X] Create repository on gitlab, set access as internal.
* [X] Upload code to gitlab.
* [X] Review code in gitlab, make sure there is nothing confidential (such as branches or tags having customer names).
* [X] Decide whether we want to have gitlab issues enabled. Most likely the answer will be yes, because with the intention to engage community more in the project, we need to have a process going. Having issues would be essential part of it. Yes, we want it.
* [X] Decide if we want to migrate issues from RT. As of Jan 28, 2019, there are 637 open issues in RT. Many of them are confidential. We absolutely can't and shouldn't migrate all of them. We can consider migrating selected few, maybe dhcp-public (91 tickets) and/or dhcp-suggest (261 tickets)? Decided to NOT migrate anything. People can migrate manually those tickets they care about.
* [x] Create milestones representing upcoming releases.
* [X] Add bug/feature request templates.
* [X] Create labels that reflect our process (Doing, Review) and components (client, server, relay, build, doc, omapi, failover, ddns, etc).
* [X] Set more appropriate logo for the project.
* [X] Announce internally that we're moving to gitlab.
* [X] Write README.md file.
* [X] Write Contributor's guide (CONTRIBUTING.md)
* [X] Switch the repo to public.
* [X] Enable github synchronization (we want mirror on github).
* [X] send announcements to dhcp-users.


