This page describes ISC DHCP project workflow. Everyone who contribute to ISC DHCP project is expected to read it at least once.

# Gitlab setup
- create an account if you don't have it yet.
- go to gitlab.isc.org, click on your user icon in right top corner and click settings from the drop menu. Then click ssh keys icon on the left panel.
- There are links there that explain how to generate your keys if you somehow haven't done that already. Anyway, the idea is to paste the content of your ~/.ssh/key-name.pub file there.
- As a completely optional step, I configured my ssh client to use separate key for gitlab, by adding this to my ~/.ssh/config file:
```
Host gitlab.isc.org
IdentityFile ~/.ssh/id_ed25519_gitlab
```

# Set up your git repository

To set up a new repository do:
```
git clone git@gitlab.isc.org:isc-projects/kea.git
cd dhcp
```

ISC developers only: If you intend to salvage changes from the old internal repo, you can do the following:
```
cd dhcp
git remote add old ssh://username@repo.isc.org/proj/git/prod/dhcp.git
git fetch old
```

Now you can do:

```
# This will pull the current branch you're on from gitlab
git pull

# This will pull whatever you still need to salvage from the old internal repo
git fetch old
```

When migrating stuff between repos, DO NOT copy files over. Use cherry-pick instead.

# Getting dependencies

ISC DHCP requires [bind9](https://gitlab.isc.org/isc-projects/bind9) sources. Those are bundled in release tarballs and now also bundled into the repo in the bind directory.  The version of the bind9 tar ball in
the repo is correct for the repo.

Prior to this one had to run the script  util/bind.sh.  This script is still present, but it is no longer necessary to run it unless you are trying to build against a different version of BIND9.

# Building ISC DHCP in one go:

```
git clone git@gitlab.isc.org:isc-projects/kea.git
cd dhcp
./configure
make
sudo make install
```

# Adding an issue

- We have an old [bugs RT](https://bugs.isc.org) system with many tickets, some of them very old. We decided to not migrate them. If there are bugs reported there that you still care about, please recreate them in gitlab. I know it may be a bit annoying, but this is the only reasonable way to find out which problems people care about.

- If you have something new to report on, create an issue in gitlab: go to https://gitlab.isc.org/isc-projects/dhcp/issues and click New issue. Make sure it's not assigned to any milestone. ISC engineers review new tickets regularly and will assign to appropriate milestones. If you move the ticket yourself, nobody would even notice its existence.

# Developers: Working on an issue

- Assign the issue to yourself. You should also assign Doing label. There are two ways of doing that. First, you can add the label manually when browsing an issue. Alternatively, you can go to https://gitlab.isc.org/isc-projects/dhcp/boards and move your issue to appropriate stage. 

- Open the issue page, e.g. https://gitlab.isc.org/isc-projects/dhcp/issues/3 and click create merge request. a branch will be created for you. You can click on the small triangle icon next to create merge request and specify a shorter, more convenient branch name. Make sure it starts with the issue number, though. Alternatively, if you have commit rights you can push the branch first and go to [DHCP merge requests](https://gitlab.isc.org/isc-projects/dhcp/merge_requests) page and create new MR for the branch you just pushed.

- ```git pull```. Note the branch that was created will be there. Check it out and start working on the code. If you dislike the branch name being too long, consider using shorter description for the issue. Also, if you have bash-completion package installed (and using bash), you can type: git checkout 3, and hit tab. The full name of the branch will be completed for you.

- Commit your changes, push it on that branch.

- Once you are done, do the following: unassign yourself from the issue and MR, remove "doing" label, add "review" label. The doing and review labels should be put on both the issue and the MR. This indicates the MR is ready for a review. This looks like a duplication of work, but it serves different purposes. The issues are tracked using a board (see [this 1.5 board](https://gitlab.isc.org/isc-projects/kea/boards?scope=all&utf8=%E2%9C%93&state=opened&milestone_title=Kea1.5) for example). Once you assign a label to an issue it is shown in the appropriate column. Now, the review label on MR indicates that particular one is ready for review. The illusion of duplication disappears when there are multiple MRs assigned to a single issue.

# Reviewing an issue

- as a reviewer: go to Issues->Board page. Look for an issue in review state that has no assignee. Assign to yourself. Review. Put some snarky comments. Add some more comments. Once you're done, reassign back to the developer.

- as a developer: look for issues that are assigned to you. Do your best with addressing the comments. Push your improvements to the branch. Once done reassign back to the reviewer. Do not merge until the reviewer says the code is ready.

- as a reviewer: Once all your comments are addressed, put a note that the code is ready to merge, open the MR, edit it and click on "remove WIP status". Removal of the WIP status is a clear indication the code is ready to go.

# Merging code

- DHCP project is currently set up in a way that allows only fast-forward merges. While it is not the only possible option, it looks like the right way to go. It keeps the repo history much cleaner.

- Currently we don't have any CI integrated with gitlab. Before merging, please run unit-tests manually. We hope to add some CI.

- Once you push your changes to master, gitlab should close the merge request and the ticket.

- Don't forget to update the commit-id in ChangeLog. Use MR (Merge Request) number, not issue number. The reason for this is that there may be multiple MRs for one issue. Also, in case we import pull requests from github, there may be no issue at all.

- Write down your thoughts. If needed, please update this page.

## Using labels
One major difference, advantage or a flaw (depending on your personal preference) of gitlab is its label system. While the number of labels is small now, it is expected to grow over time. Adding labels to your issue lets other people find your issue quicker and correlate it with other related issues.

Violet labels are for priority. There are four priority labels: critical, high, medium and low.

Orange labels are for components.

Green labels designate type and state of the issue (feature request, QA needed, etc)

Red labels are for things that should stand out. Currently there's one label: bug.

## Getting write access to the repo

Once you submit several good patches and you are interested in stepping up, it is possible to get write access to the DHCP repository. This is a new experiment that we decided to try to get more community involvement. The candidates will be expected to follow the process and will have rights and responsibilities of a regular ISC engineer. That means following the normal review procedure, review code, merge code to master only after completing review etc. Please contact one of the ISC staff if you are interested.