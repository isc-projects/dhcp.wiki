Kea migration assistant is an **experimental** extension to ISC DHCP that is able to read an isc-dhcp configuration and produce a JSON configuration in Kea format. It should be used as a standalone configuration conversion utility. We do not recommend running this code in a production DHCP server.

## Limitations

This utility will translate most of your ISC DHCP configuration to the appropriate format for Kea. Some elements of your ISC DHCP configuration can not be automatically translated to Kea format. This is because some features of ISC DHCP are either unsupported, or it work in a different way in Kea vs. ISC DHCP. Where the utility is unable to translate the configuration, it will insert messages highlighting what was not translated, with references to issues in the Kea Gitlab that provide more detail. These sections of the configuration will require manual review and adjustment.

This Kea migration assistant does not translate the current DHCP lease file. This works on configuration files only. There is an experimental project to translate lease files [here](https://gitlab.isc.org/isc-projects/keama-leases).

## Packaged version

ISC provides RPM/deb packages on cloudsmith here: https://cloudsmith.io/~isc/repos/keama/packages/

## Building from source code:

```bash
# First fetch the source tarball by http download or ftp. KeaMA is a feature of ISC DHCP 4.4.
https://ftp.isc.org/isc/dhcp/4.4.2/

or 

# wget -nd https://ftp.isc.org/isc/dhcp/4.4.2/dhcp-4.4.2.tar.gz

#  Unarchive it
tar -xf dhcp-migration-assistant.tar.gz

#  Change into main directory
cd dhcp-migration-assistant

#  Configure the build.  If you want to install it somewhere specific use --prefix=<path> parameter
./configure  

# Change into the migration assistant directory
cd keama

# Run make to build keama
make

# Install it (optional)
sudo make install  
```

## Using the Kea migration assistant:

```bash
./keama  {-4|-6} -i <input ISC DHCP config file> -o <output file> -l <hook library path>
```