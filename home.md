Welcome to the ISC DHCP project wiki. ISC DHCP is a very mature, stable project in maintenance mode.

* [Current version tar balls - 4.4 and 4.1 trains](https://www.isc.org/downloads/)
* [ISC Knowledgebase with many articles on using ISC DHCP](kb.isc.org)
* [Contributer's Guide](https://gitlab.isc.org/isc-projects/dhcp/blob/master/CONTRIBUTING.md)
* [Using DHCP repo with gitlab](processes/gitlab howto)
* [ISC DHCP man pages](https://kb.isc.org/docs/aa-00333#)
* [Random open source tools for use with ISC DHCP](https://www.isc.org/free-tools-and-resources-for-isc-dhcp/)
* [gitlab migration](gitlab migration) - anything related to migrating from repo.isc.org and potentially RT.
* [Historical bug tracker for ISC DHCP](https://bugs.isc.org) - we have not migrated our backlog of issues to this Gitlab repo, and we are unlikely to migrate all of these, because they go back years. Please do not attempt to enter new issues into bugs.isc.org - enter new issues [here](https://gitlab.isc.org/isc-projects/dhcp/issues).
* [Kea migration assistant](kea-migration-assistant) - experimental tool for translating ISC DHCP configuration files to Kea configuration files.
* [IscDhcpSurvivalGuidePublic.pdf](uploads/c8f0c3ba85ce94a3e2a7c0c895deb559/IscDhcpSurvivalGuidePublic.pdf) - While not complete, this document discusses a lot of the nitty gritty in the ISC DHCP code base.
* [HankinsHandoff-ISCDHCP-2010.pdf](uploads/0d0e4ecfcb0a22ee1b7e869497ce3d29/HankinsHandoff-ISCDHCP-2010.pdf) - this is mostly of historical interest, but there are some pretty humorous discussions of issues with the DHCP protocol included. 